use std::convert::From;
use std::vec::Vec;

/// A macro to write rust-usable pointers in a somewhat nicer way
///
/// # Example:
///
/// ```rust
/// ptr!(0xDEADBEEF, u8) = 255
/// ```
/// Taken from https://github.com/boatboatboatboat/chiter
#[macro_export]
macro_rules! ptr {
    ($address:expr, $type:ty) => {
        *($address as *mut $type)
    };
}

///Reads an Value from the specified adress
/// 
/// # Arguments
/// 
/// * `address` Address of the starting point of the Value
/// * `length` Size of the Value. Note that this is not actually the size of the value, but the size of bytes needed for the From::from conversion
/// 
/// # Example
/// ```
/// let data_array = read_memory<Vec<u8>>(0x6473AA4, 24);
/// ```
pub fn read_memory<T: From<Vec<u8>>>(address: usize, length: usize) -> T {
    let mut result = Vec::<u8>::new();
    for index in (0..length).rev() {
        unsafe { result.push(ptr!(address + index, u8)) }
    }
    T::from(result)
}

///writes an Value to the specified adress
/// 
/// # Arguments
/// 
/// * `address` Address of the starting point of the Value
/// * `Object` An object that implements Into<Vec<u8>>
/// 
/// # Example
/// ```
/// //remember, sample_object can be any type that implements Into<Vec<u8>>
/// write_memory(0x6473AA4, sample_object)
/// ```
pub fn write_memory<T: Into<Vec<u8>>>(address: usize,object: T){
    let vector = object.into();
    for (idx, byte) in vector.into_iter().enumerate(){
        unsafe { ptr!(address + idx, u8) = byte};
    }
}