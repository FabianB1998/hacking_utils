
/// A macro to convert a pointer into a function
///
/// # Example:
/// ```c
/// // This code is in C.
/// int add_one(int thing) {
///     return thing + 1;
/// }
/// ```
/// ```rust
/// // This code is in Rust. 0xDEADBEEF is the address where add_one starts.
/// let add_one = unsafe { make_fn!(0xDEADBEEF, i32, i32) };
///
/// assert_eq!(add_one(400), 401);
/// ```
/// Taken from https://github.com/boatboatboatboat/chiter
#[macro_export]
macro_rules! make_fn {
    ($address:expr, $returntype:ty) => {
        std::mem::transmute::<*const usize, fn() -> $returntype>($address as *const usize)
    };
    ($address:expr, $returntype:ty, $($argument:ty),*) => {
        std::mem::transmute::<*const usize, fn($($argument,)*) -> $returntype>($address as *const usize)
    }
}